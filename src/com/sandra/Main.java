package com.sandra;

import com.sandra.guitars.*;
import java.util.ArrayList;

public class Main {
    private static ArrayList<Guitar> guitarCollection = new ArrayList<>();
    private static Guitar myAcoustic = new AcousticGuitar("Takamine", "EF341SC", 1981);
    private static Guitar myElectric = new ElectricGuitar("Fender", "Telecaster/Esquire", 1973, "51Nocaster");
    private static Guitar myElectroAcoustic = new ElectroAcousticGuitar("Yamaha", "A5R", 2022, "SRT2");
    private static Amplifier myAmplifier = new Amplifier("Orange", "Super Crush", 90, 100);

    public static void testGuitars () {
        guitarCollection.add(myAcoustic);
        guitarCollection.add(myElectric);
        guitarCollection.add(myElectroAcoustic);

        for (Guitar guitar : guitarCollection) {
            guitar.makeSound();
        }
    }

    public static void testAmp () {
        myAmplifier.makeSound();
        myAmplifier.switchOn();
        myAmplifier.makeSound();

        for (Guitar guitar : guitarCollection) {
            System.out.println("Testing my " + guitar.getBrand() + " with my amplifier:");
            try {
                myAmplifier.connectGuitar(guitar);
            } catch (InvalidGuitarException e) {
                System.out.println(e);
            }
            myAmplifier.makeSound();
            myAmplifier.disconnectGuitar();
            myAmplifier.makeSound();
        }

        myAmplifier.switchOff();
        myAmplifier.makeSound();
    }

    public static void main(String[] args) {

        testGuitars();
        testAmp();


    }
}
