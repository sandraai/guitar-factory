package com.sandra.guitars;

public class AcousticGuitar extends Guitar{

    public AcousticGuitar (String brand, String model, int manufacturedYear) {

        super(brand, model, manufacturedYear);
    }

    @Override
    public void makeSound () {

        System.out.println("sparkly crispy tones");
    }
}
