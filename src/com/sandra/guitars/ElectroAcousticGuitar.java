package com.sandra.guitars;

public class ElectroAcousticGuitar extends Guitar{
    private String pickup;

    public ElectroAcousticGuitar (String brand, String model, int manufacturedYear, String pickup) {
        super(brand, model, manufacturedYear);
        this.pickup = pickup;
    }

}
