package com.sandra.guitars;

public class InvalidGuitarException extends Exception {
    public InvalidGuitarException (String message) {
        super(message);
    }
}
