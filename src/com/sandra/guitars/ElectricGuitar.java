package com.sandra.guitars;

public class ElectricGuitar extends Guitar{
    private String pickup;

    public ElectricGuitar (String brand, String model, int manufacturedYear, String pickup) {
        super(brand, model, manufacturedYear);
        this.pickup = pickup;
    }

    @Override
    public void makeSound () {

        System.out.println("fat twang");
    }
}
