package com.sandra.guitars;

public class Amplifier implements Playable{
    private String brand;
    private String model;
    private int volume;
    private int wattage;

    private boolean switchedOn = false;
    private Guitar guitarPluggedIn = null;

    public Amplifier (String brand, String model, int volume, int wattage) {
        this.brand = brand;
        this.model = model;
        this.volume = volume;
        this.wattage = wattage;
    }

    public void switchOn () {
        switchedOn = true;
    }

    public void switchOff () {
        switchedOn = false;
    }

    public void connectGuitar (Guitar guitar) throws InvalidGuitarException {
        if (!(guitar instanceof ElectricGuitar) && !(guitar instanceof ElectroAcousticGuitar))
            throw new InvalidGuitarException("That kind of guitar cannot be plugged in!");
        guitarPluggedIn = guitar;
    }

    public void disconnectGuitar () {
        guitarPluggedIn = null;
    }

    public void makeSound () {
        if (switchedOn) {
            if ( guitarPluggedIn == null) {
                System.out.println("STATIC NOISES");
            } else {
                guitarPluggedIn.makeSound();
                System.out.println(volume + " dB");
            }
        } else {
            System.out.println("*silence*");
        }

    }
}
