package com.sandra.guitars;

public abstract class Guitar implements Playable{
    private String brand;
    private String model;
    private int manufacturedYear;

    public Guitar (String brand, String model, int manufacturedYear) {
        this.brand = brand;
        this.model = model;
        this.manufacturedYear = manufacturedYear;
    }

    public String getBrand () {
        return brand;
    }

    public void makeSound () {
        System.out.println("pling plong");
    }
}
